
template <class T>
Conjunto<T>::Conjunto() : _raiz(nullptr),_elems(0) {
//
}
//---------------------------------------------------------------------------------------------------
template <class T>
void Conjunto<T>::destruir( Nodo * n) {
    if (n!= nullptr) {
        destruir(n->izq);
        destruir(n->der);
        delete n;
    }
}

template <class T>
Conjunto<T>::~Conjunto() {

    destruir(_raiz);
    _raiz = nullptr;
    _elems = 0;
}
//---------------------------------------------------------------------------------------------------
template <class T>
bool Conjunto<T>::auxPertenece(Nodo * ptr, const T & clave) const {
    if( ptr == nullptr){ // si es vacio
        return  false;
    }else if(clave < ptr->valor){
        // si el valor es menor a la raiz, voy por subarbol izq
            return ( auxPertenece(ptr->izq, clave));
        }else if (clave > ptr->valor)// si el valor es mayor a la raiz, voy por subarbol der
            return (auxPertenece(ptr->der, clave));
        else
            return  true;
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    return auxPertenece(_raiz,clave);
}
//---------------------------------------------------------------------------------------------------
//Pre: ptr!= nullptr

template <class T>
void Conjunto<T>::auxInsertar(Nodo *& ptr, const T & clave, Nodo* padre){
    if(clave < ptr->valor){ // si el valor es menor a la raiz, voy por subarbol izq
           if(ptr->izq == nullptr)
                ptr->izq = new Nodo (clave,ptr);
           else
               auxInsertar( ptr->izq, clave,ptr );
    }
    else{ // si el valor es mayor a la raiz, voy por subarbol der
         if (ptr->der == nullptr)
                 ptr->der = new Nodo (clave,ptr);
         else
               auxInsertar( ptr->der, clave,ptr );
    }
}
/*template <class T>
void Conjunto<T>::auxInsertar(Nodo  *&ptr, const T & clave, Nodo* padre){
    if(ptr == nullptr){ // si es un arbol vacio
        ptr = new Nodo(clave,padre); //leak
    }
    else if(clave < (ptr)->valor){ // si el valor es menor a la raiz, voy por subarbol izq
        auxInsertar( ((ptr)->izq), clave,ptr );
    }
    else if (clave > (ptr)->valor){ // si el valor es mayor a la raiz, voy por subarbol der
        auxInsertar( ((ptr)->der), clave,ptr );
    }
}*/


template <class T>
void Conjunto<T>::insertar(const T& clave) {
    if(!pertenece(clave)){
        if( _raiz == nullptr){
            _raiz = new Nodo(clave, nullptr);//???
        }
        else
          auxInsertar(_raiz, clave, nullptr);

        _elems++;
        }
}
//---------------------------------------------------------------------------------------------------
template<class T>
void Conjunto<T>::DestruirNodo(Nodo * n){
    n->izq = nullptr;
    n->der = nullptr;

    delete n;
}
template<class T>
void Conjunto<T>::reemplazar(Nodo* &arbol, Nodo* &NuevoNodo){ //usa padre
    if(arbol->padre){
        if(arbol->padre->izq && arbol->valor == arbol->padre->izq->valor){
            arbol->padre->izq = NuevoNodo;
        }
        else if(arbol->padre->der && arbol->valor == arbol->padre->der->valor){
            arbol->padre->der = NuevoNodo;
        }
    }
    else{
        _raiz = NuevoNodo;
    }
    if(NuevoNodo){
        NuevoNodo->padre = arbol->padre;
    }

}

template<class T>
void Conjunto<T>::eliminarNodo(Nodo* aeliminar){

    if(aeliminar->izq == nullptr){ //solo hijo Der
        reemplazar(aeliminar,aeliminar->der);
        DestruirNodo(aeliminar);
    }
    else if(aeliminar->der == nullptr){ //solo hijo izq
        reemplazar(aeliminar,aeliminar->izq);
        DestruirNodo(aeliminar);
    } else{
        Nodo* pre = nullptr;
        Nodo* suc = nullptr;
        buscarPreYSubSucesor(_raiz,pre,suc,aeliminar->valor);

        if(suc->padre->valor != aeliminar->valor){
            reemplazar(suc,suc->der);
            suc->der = aeliminar->der;
            if(aeliminar->der)
                suc->der->padre = suc;
        }
        reemplazar(aeliminar,suc);
        suc->izq = aeliminar->izq;
        suc->izq->padre = suc;
        DestruirNodo(aeliminar);
    }

    /*else if(aeliminar->valor == _raiz->valor){
        _raiz = nullptr;
    }*/

    /*
    else{ // no tiene hijos
        if(aeliminar->padre){
        if(aeliminar->padre->izq == aeliminar){
            aeliminar->padre->izq = nullptr;
        }else if(aeliminar->padre->der == aeliminar){
            aeliminar->padre->der = nullptr;
        }
        }else{
            _raiz = nullptr;
        }
    }*/
}

template<class T>
void Conjunto<T>::eliminarDelArbol(Nodo* arbol,const T& clave){
    if(clave < arbol->valor){ //si el nodo que quiero borrar es más chico que la raíz del árbol, voy por izq
        eliminarDelArbol(arbol->izq,clave);
    }
    else if( clave > arbol->valor ){ //si el nodo que quiero borrar es más grande que la raíz del árbol, voy por der
        eliminarDelArbol(arbol->der,clave);
    }
    else {
        eliminarNodo(arbol);
    }
}

template <class T>
void Conjunto<T>::remover(const T& clave) {
    if(pertenece(clave)){
        eliminarDelArbol(_raiz,clave);
        _elems--;
    }
}
//---------------------------------------------------------------------------------------------------
template <class T>
void Conjunto<T>::buscarPreYSubSucesor(Nodo* raiz, Nodo* &pre,Nodo* &suc,const T& clave){
    if (raiz == nullptr){
        return;
    }
    if(raiz->valor == clave){
        //el maximo valor en el arbol izquierdo es el predecesor
        if(raiz->izq != nullptr){
            Nodo* tmp = raiz->izq;
            while(tmp->der){
                tmp = tmp->der;
            }
            pre = tmp;
        }
        // el minimo valor en el árbol derecho es el sucesor
        if(raiz->der != nullptr){
            Nodo* tmp = raiz->der;
            while(tmp->izq){
                tmp = tmp->izq;
            }
            suc = tmp;
        }
        return;
    }
    //si la clave es menor que la raíz voy por el subarbol izquierdo
    if (raiz->valor > clave)
    {
        suc = raiz ;
        buscarPreYSubSucesor(raiz->izq, pre, suc, clave) ;
    }
    else //sino voy por el subarbol derecho
    {
        pre = raiz ;
        buscarPreYSubSucesor(raiz->der, pre, suc, clave) ;
    }
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) { //más chico del subárbol derecho
    Nodo* pre = nullptr;
    Nodo* suc = nullptr;
    buscarPreYSubSucesor(_raiz, pre, suc,clave);
    if(suc != nullptr){
        return suc->valor;
    }
}
//---------------------------------------------------------------------------------------------------
template <class T>
const T& Conjunto<T>::auxMinimo(Nodo * ptr) const {
    while(ptr->izq != nullptr){
        ptr = ptr->izq;
    }
    return ptr->valor;
}

template <class T>
const T& Conjunto<T>::minimo() const { //mas a la izquierda
    return auxMinimo(_raiz);
}
//---------------------------------------------------------------------------------------------------
template <class T>
const T& Conjunto<T>::auxMaximo(Nodo * ptr) const{
    while(ptr->der != nullptr){
        ptr = ptr->der;
    }
    return ptr->valor;
}

template <class T>
const T& Conjunto<T>::maximo() const { // mas a la derecha
    return auxMaximo(_raiz);
}
//---------------------------------------------------------------------------------------------------
template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return _elems;
}
//---------------------------------------------------------------------------------------------------
template <class T>
void Conjunto<T>::mostrar(std::ostream& os)  {
    mostrar_(_raiz,0,os);
}

template<class T>
void Conjunto<T>::mostrar_(Conjunto::Nodo *&r, int space,std::ostream& os)  {
    if (r!= nullptr){
        mostrar_(r->izq,space+4,os);
        mostrar_(r->der,space+4,os);
        if (space)
            os << std::setw(space) << ' ';

        os<<r->valor<<'\n';
    }
}