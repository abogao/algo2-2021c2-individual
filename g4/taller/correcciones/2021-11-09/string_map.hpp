template <typename T>
string_map<T>::string_map() : raiz(new Nodo()), _size(0){

}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.


template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    //mismas claves con las mismas definiciones.
    //primero tengo que borrar todo lo que hay en this.
    destruir(this->raiz);
    raiz = new Nodo(d.raiz);
    _size = d._size;
    return *this;
}
template <typename T>
void string_map<T>::destruir(Nodo* n){
    if (n!= nullptr) {
        for(int i=0;i < 256;i++) {
            if(n->siguientes[i]!= nullptr) {
                destruir(n->siguientes[i]);
            }
        }
        delete n->definicion;
        delete n;
    }
}

template <typename T>
string_map<T>::~string_map(){
    destruir(raiz);
}
template <typename T>
void string_map<T>::insert(const pair<string, T>& a){
    // si el trie esta vacio creo un nodo al que apunta a la raiz.
   if(raiz == nullptr){
       Nodo* nuevo = new Nodo();
        raiz = nuevo;
    }
    //busco el lugar del trie en que debe ir la nueva clave.
        Nodo* actual = raiz;
        int i = 0;
        while(i < a.first.length()){
            char c = (a.first)[i];
         if (actual->siguientes[int(c)] == nullptr){
                Nodo* nn = new Nodo();
                actual->siguientes[int(c)] = nn;
            }
            actual = actual->siguientes[int(c)];
            i++;
        }
    T* significado = new T(a.second);
            if (actual->definicion != nullptr) {
        delete actual->definicion;
        actual->definicion = significado;
    }else{
        actual->definicion = significado;
        _size ++;
    }
}

template <typename T>
T& string_map<T>::operator[](const string& clave){
    if(count(clave) == 0){
        string c = clave;
        insert(make_pair(c,0));
    }
        return at(clave);
}

template <typename T>
int string_map<T>::count(const string& clave) const{
    Nodo* actual = raiz;
    int i = 0;
    while(i < clave.length()){
        char c = clave[i];
        if(actual == nullptr){
         return 0;
        }else if (actual->siguientes[int(c)] == nullptr) {
            return 0;
         }else {
            actual = actual->siguientes[int(c)];
            i++;
        }
    }
    if (actual->definicion){
        return 1;
    }else{
        return 0;
    }
}

template <typename T>
const T& string_map<T>::at(const string& clave) const {
    Nodo* actual = raiz;
    int i = 0;
    while(i < clave.length()){
        char c = clave[i];
        if(actual->siguientes[int(c)] != nullptr){
            actual = actual->siguientes[int(c)];
        }
        i++;
    }
    return *(actual->definicion);
}

template <typename T>
T& string_map<T>::at(const string& clave) {
    Nodo* actual = raiz;
    int i = 0;
    while(i < clave.length()){
        char c = clave[i];
        if(actual->siguientes[int(c)] != nullptr){
            actual = actual->siguientes[int(c)];
        }
        i++;
    }
    return *(actual->definicion);
}

template <typename T>
void string_map<T>::erase(const string& clave) {
    Nodo* ultimo = raiz;
    Nodo* actual = raiz;
    int ultimo_indice = 0;
    for (int i = 0; i < clave.size(); ++i) {
        int c = int(clave[i]);
        if (actual->siguientes[c]==nullptr)
            return;
        if (actual->siguientes[c]->definicion != nullptr){
            ultimo = actual;
            ultimo_indice = i+1;
        } else {
            for (int j = 0; j < actual->siguientes.size(); ++j) {
                if (j != c && actual->siguientes[j] != nullptr){
                    ultimo = actual;
                    ultimo_indice = i+1;
                }
            }
        }
        actual = actual->siguientes[c];
    }
    delete actual->definicion;
    actual->definicion = nullptr;
    bool hijos = false;
    for (int i = 0; i < actual->siguientes.size(); ++i) {
        if (actual->siguientes[i]!= nullptr) {
            hijos = true;
            break;
        }
    }
    if (!hijos){
        int c = int(clave[ultimo_indice]);
        Nodo* siguiente = ultimo->siguientes[c];
        ultimo->siguientes[c] = nullptr;
        ultimo = siguiente;
        ultimo_indice++;
        for (int i = ultimo_indice; i < clave.size(); ++i) {
            c = int(clave[ultimo_indice]);
            siguiente = ultimo->siguientes[c];
            delete ultimo;
            ultimo = siguiente;
        }
        delete ultimo;
    }
    _size--;
}

template <typename T>
int string_map<T>::size() const{
    return this->_size;
}

template <typename T>
bool string_map<T>::empty() const{
    return 0 == this->_size;
}
