#include "Lista.h"

Lista::Lista() {
    _long = 0;
    _primero = new Nodo();
    _ultimo = new Nodo();
    _primero->siguiente = _ultimo;
    _primero->anterior = nullptr;
    _ultimo->siguiente = nullptr;
    _ultimo->anterior = _primero;
}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    Nodo* dl = _primero;
    while(dl != nullptr){
        Nodo* temp = dl->siguiente;
        delete dl;
        dl = temp;
    }
}
//hacer funcion auxiliar con el codigo repetido
Lista& Lista::operator=(const Lista& aCopiar) {
    Nodo* dl = _primero->siguiente;
    for(int i=0; i < longitud(); i++){
        Nodo* temp = dl->siguiente;
        delete dl;
        dl = temp;
    }
    _long = 0;
    _primero->siguiente = _ultimo;
    _ultimo->anterior = _primero;

    for (int i = 0; i < aCopiar.longitud(); i++) {
        int e = aCopiar.iesimo(i);
        this->agregarAtras(e);
    }
    return *this;
}

void Lista::agregarAdelante(const int& elem) { //cuando es vacía
    Nodo* n = new Nodo();
    n->valor = elem;
   /* if(longitud() == 0){
        _primero->siguiente = n;
        _ultimo->anterior = n;
        n->siguiente = _ultimo;
        n->anterior = _primero;

    }else{*/
        n->anterior = _primero;
        n->siguiente = _primero->siguiente;
        _primero->siguiente->anterior = n;
        _primero->siguiente = n;
    //}
    _long++;
}

void Lista::agregarAtras(const int& elem) {
    Nodo* n = new Nodo();
    n->valor = elem;
    /*if(longitud() == 0){
        n->siguiente= _ultimo;
        n->anterior = _primero;
        _primero->siguiente = n;
        _ultimo->anterior = n;//
    }else {*/
        n->anterior = _ultimo->anterior;
        n->siguiente = _ultimo;
        _ultimo->anterior->siguiente = n; //?
        _ultimo->anterior = n;
   // }
    _long++;
}

void Lista::eliminar(Nat i) { //ver primero y ultimo, y cuando hay un solo elem
   /* Nodo* ptr = _primero->siguiente;
    if(longitud() == 1 && i==0){
        delete &iesimo(0);
        _primero->siguiente = _ultimo;
        _ultimo->anterior = _primero;
    }else if(i == 0 && longitud() > 1){
        ptr = ptr->siguiente;
         delete &iesimo(i);
        _primero->siguiente =  ptr;
        ptr->anterior = _primero;
    }else if(i == longitud() - 1 && longitud() > 1){
        for (int j = 0; j < longitud() - 1; j++) {
            ptr = ptr->siguiente;
        }
        ptr->anterior = _ultimo;
        _ultimo->anterior = ptr->anterior;
        delete ptr;
    }else{
        for (int j = 0; j < i; j++) {
            ptr = ptr->siguiente;
        }
        ptr->anterior->siguiente = ptr->siguiente;
        ptr->siguiente->anterior = ptr->anterior;
       delete ptr;
    }_long--;*/

    Nodo* ptr = _primero->siguiente;
     if(ptr != nullptr) {
         for (int j = 0; j < i; j++) {
             ptr = ptr->siguiente;
         }
         ptr->anterior->siguiente = ptr->siguiente;
         ptr->siguiente->anterior = ptr->anterior;
         _long--;
         delete ptr;
     }
}
int Lista::longitud() const {
    return _long;
}

const int& Lista::iesimo(Nat i) const { //?
    if( i < longitud()){
       Nodo* ptr = _primero->siguiente;
        for (int j = 0; j < i; j++) {
            ptr = ptr->siguiente;
        }
        return ptr->valor;
    }
    assert(false);
}
int& Lista::iesimo(Nat i) {
    // Completar (hint: es igual a la anterior...)
    if(i < longitud()){
        Nodo* ptr = _primero->siguiente;
        for (int j = 0; j < i; j++) {
            ptr = ptr->siguiente;
        }
        return ptr->valor;
    }
    assert(false);
}
void Lista::mostrar(ostream& o) {
    Nodo* ptr = _primero->siguiente;
    while(ptr != nullptr){
        cout<< &ptr <<endl;
        ptr = ptr->siguiente;
    }
}
