#include <iostream>
#include <list>
using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    Fecha(int mes, int dia);
    int mes();
    int dia();
    void incrementar_dia();
    #if EJ >= 9 // Para ejercicio 9
    bool operator==(Fecha o);
    bool operator<(Fecha o);
    #endif

  private:
    int mes_;
    int dia_;
};
Fecha::Fecha(int mes, int dia){
    if (mes > 0 && mes <13){
        mes_ = mes;
        if(dia > 0 && dia <= dias_en_mes(mes)){
            dia_ = dia;
        }
    }
};
int Fecha::mes(){
    return mes_;
}
int Fecha::dia(){
    return dia_;
}
// ej 8
ostream& operator<<(ostream& os, Fecha f) {
    os << f.dia() << "/" << f.mes();
    return os;
}
bool Fecha::operator<(Fecha o) {
    bool menor_dia = this->dia() < o.dia();
    bool menor_mes = this->mes() < o.mes();
    return menor_dia || menor_mes;
}
#if EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    bool igual_mes = this->mes() == o.mes();
    return igual_dia && igual_mes;
}
#endif
// ej 10
void Fecha::incrementar_dia(){
    if(dia_ > 0 && dia_ < dias_en_mes(mes_)){
        dia_= dia_ +1;
    }else if(dia_ == dias_en_mes(mes_)){
        dia_ = 1;
        if(mes_ >0 && mes_ < 12){
            mes_++;
        }else if(mes_==12){
            mes_ = 1;
        }

    }
}
// Ejercicio 11, 12

// Clase Horario
class Horario{
public:
    Horario(uint hora, uint min);
    uint hora();
    uint min();
    bool operator==(Horario h);
    bool operator<(Horario h);

private:
    uint hora_;
    uint min_;
};
Horario::Horario(uint hora, uint min){
    if(hora >= 0 && hora < 24){
        hora_ = hora;
    }if(min>= 0 && min < 60){
        min_ = min;
    }
}

uint Horario::hora(){
    return hora_;
}
uint Horario::min(){
    return min_;
}

ostream& operator<<(ostream& os, Horario h) {
    os << h.hora() << ":" << h.min();
    return os;
}

bool Horario::operator==(Horario h) {
    bool igual_min = this->min() == h.min();
    bool igual_hora = this->hora() == h.hora();
    return igual_min && igual_hora;
}

bool Horario::operator<(Horario h) {
    bool menor_hora = this->hora() < h.hora();
    bool menor_min = this->min() < h.min();

    return (menor_hora || menor_min) ;
}
// Ejercicio 13

// Clase Recordatorio
class Recordatorio{
    public:
        Recordatorio( Fecha f, Horario h, string mensaje);
        string mensaje();
        Fecha f();
        Horario h();
        bool operator<(Recordatorio r);

    private:
        string mensaje_;
        Horario h_;
        Fecha f_;
};
Recordatorio::Recordatorio( Fecha f, Horario h, string mensaje) : f_(f),h_(h)  {
    mensaje_ = mensaje;
};

Fecha Recordatorio::f() {
    return f_;
}
Horario Recordatorio::h() {
    return h_;
}
string Recordatorio::mensaje() {
    return mensaje_;
}
bool Recordatorio::operator<(Recordatorio r) {
    bool menor_horario = this->h() < r.h();
    bool menor_fecha = this->f() < r.f();

    return (menor_horario || menor_fecha) ;
}
ostream& operator<<(ostream& os, Recordatorio r) {
    os << r.mensaje() << " @ " << r.f() << " " << r.h();
    return os;
}


// Ejercicio 14

// Clase Agenda

class Agenda{
    public:
        Agenda(Fecha fecha_inicial);
        void agregar_recordatorio(Recordatorio rec);
        void incrementar_dia();
        list<Recordatorio> recordatorios_de_hoy();
        Fecha hoy();

    private:
        Fecha fecha_inicial_;
        list<Recordatorio> recordatorios_;

};
Agenda::Agenda(Fecha fecha_inicial) : fecha_inicial_(fecha_inicial){

};

void Agenda::agregar_recordatorio(Recordatorio rec){
    recordatorios_.push_back(rec);
}

void Agenda::incrementar_dia() {
        fecha_inicial_.incrementar_dia();
}

list<Recordatorio> Agenda::recordatorios_de_hoy(){
    recordatorios_.sort();
    list<Recordatorio> losDeHoy;
    for(Recordatorio r: recordatorios_){
        if(r.f() == hoy()){
            losDeHoy.push_back(r);
        }
    }
    return losDeHoy;
}
Fecha Agenda::hoy() {
    return fecha_inicial_;
}

ostream& operator<<(ostream& os, Agenda a) {
        os << a.hoy() << endl;
        os << "=====" << endl;
        for (Recordatorio r: a.recordatorios_de_hoy()) {
            os << r << endl;
        }
    return os;
}
